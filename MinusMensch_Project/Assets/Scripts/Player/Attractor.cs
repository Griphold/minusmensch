﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attractor : MonoBehaviour
{
    public string InputAxis;
    public AnimationCurve forceByPress;
    public float maxForceMagnitude;
    public float selfForceModifier = 0.1f;

    [Header("Component References")]
    private AreaEffector2D effector;
    private Rigidbody2D rigidBody;
    [SerializeField]
    private PolygonCollider2D attractArea;

    private Collider2D[] overlapResults = new Collider2D[5];
    private ContactFilter2D contactFilter2D;


	void Start ()
    {
        //set up component references
        effector = GetComponent<AreaEffector2D>();
        rigidBody = GetComponent<Rigidbody2D>();

        contactFilter2D.SetLayerMask(effector.colliderMask);
        contactFilter2D.useLayerMask = true;
	}
	
	void Update ()
    {
        //get trigger input 
        float triggerPress = Mathf.Abs(Input.GetAxis(InputAxis));
        effector.forceMagnitude = forceByPress.Evaluate(triggerPress) * maxForceMagnitude;
		
	}

    void FixedUpdate()
    {
        int numHits = attractArea.OverlapCollider(contactFilter2D, overlapResults);

        //Check whether any object is being attracted
        if (numHits > 0)
        {
            //if so, apply the force to ourselves to attract self to attracted object
            Vector2 forceDir = Quaternion.AngleAxis(effector.forceAngle, Vector3.back) * transform.right;
            rigidBody.AddForce(effector.forceMagnitude * selfForceModifier * forceDir);
        }
    }
}
