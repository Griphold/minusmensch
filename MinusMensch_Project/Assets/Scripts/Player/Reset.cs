﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class Reset : MonoBehaviour {
    [SerializeField]
    private GameObject PlayerPrefab;
    [SerializeField]
    private Transform Checkpoint;

    private void OnTriggerEnter(Collider col) {
        List<Transform> cameraList = Camera.main.GetComponent<TrackMultiple>().toTrack;
        if (col.GetComponent<PlayerController>() != null)
            Destroy(col);
        cameraList.Clear();
        GameObject player = Instantiate(PlayerPrefab, Checkpoint);
        cameraList.Add(player.transform);
    }
}
