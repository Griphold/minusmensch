﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Foot : MonoBehaviour {
    public OnCollisionEvent OnCollision;

    [System.Serializable]
    public class OnCollisionEvent : UnityEvent<Collision2D>
    {

    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionStay2D(Collision2D collision)
    {
        OnCollision.Invoke(collision);
    }
}
