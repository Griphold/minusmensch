﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController: MonoBehaviour {

    public Leg leftLeg;
    public Leg rightLeg;

    //components
    private Rigidbody2D rigidBody;

    [System.Serializable]
    public class Leg
    {
        private PlayerController parent;

        [SerializeField]
        private GameObject legObject;
        
        private RelativeJoint2D joint;
        private Rigidbody2D rigidBody;

        [SerializeField]
        private Foot foot;

        [SerializeField]
        private string InputAxisX;
        [SerializeField]
        private string InputAxisY;
        [SerializeField]
        private string TriggerAxis;
        private float triggerAxisInput;

        public float MaximumAngleSpeed;

        public bool requestFreeze = false;

        //fixedjoint on foot
        public bool isFixed = false;
        private FixedJoint2D fixedJoint;
        private Vector2 jointNormal;

        public void Init(PlayerController parent)
        {
            this.parent = parent;

            joint = legObject.GetComponent<RelativeJoint2D>();
            rigidBody = legObject.GetComponent<Rigidbody2D>();
            foot.OnCollision.AddListener(OnFootCollisionStay);
        }

        public void Update()
        {
            float xInput = Input.GetAxis(InputAxisX);
            float yInput = Input.GetAxis(InputAxisY);

            if(isFixed)
            {
                //clamp input way from wall when fixed

            }

            Vector2 input = Vector2.ClampMagnitude(new Vector2(xInput, yInput), 1);
            if(input.SqrMagnitude() > 0.05f * 0.05f)
            {
                Vector2 currentDirection = Quaternion.AngleAxis(joint.angularOffset % 360, Vector3.back) * -parent.transform.up;
                float relativeAngle = -Vector2.SignedAngle(currentDirection, input);

                float clampedAngle = Mathf.Clamp(relativeAngle, -MaximumAngleSpeed, MaximumAngleSpeed);

                joint.angularOffset += clampedAngle;

                requestFreeze = true;
                
            }
            else
            {
                requestFreeze = false;
            }

            triggerAxisInput = Mathf.Abs(Input.GetAxis(TriggerAxis));

            if (!isFixed)
            { 
                foot.transform.rotation = legObject.transform.rotation;
            }
            else
            {
            }
            
            if (triggerAxisInput <= 0.1f && isFixed)
            {

                Rigidbody2D footRigidbody = foot.GetComponent<Rigidbody2D>();
                footRigidbody.constraints = RigidbodyConstraints2D.None;

                Destroy(fixedJoint);
                isFixed = false;
                
            }
        }

        public void SetGravityScale(float value)
        {
            legObject.GetComponent<Rigidbody2D>().gravityScale = value;
            foot.GetComponent<Rigidbody2D>().gravityScale = value;
        }

        public void OnFootCollisionStay(Collision2D collision)
        {
            
            //if pull trigger and not fixed
            if (triggerAxisInput > 0 && !isFixed)
            {
                //check contact points
                ContactPoint2D[] contacts = new ContactPoint2D[10];
                int numOfContacts = collision.GetContacts(contacts);

                if (numOfContacts >= 1)
                {
                    //calculate average normal
                    Vector2 normalToUse = contacts[0].normal;

                    //rotate foot to normal
                    foot.transform.eulerAngles = new Vector3(foot.transform.eulerAngles.x, foot.transform.eulerAngles.y, Vector2Util.clockwiseAngle(Vector2.up, normalToUse));

                    //add fixed joint
                    foot.StartCoroutine(AttachFixedJoint(collision));

                    //set bool
                    isFixed = true;
                }
            }
        }

        private IEnumerator AttachFixedJoint(Collision2D collision)
        {
            yield return new WaitForFixedUpdate();

            Rigidbody2D footRigidbody = foot.GetComponent<Rigidbody2D>();
            footRigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;

            fixedJoint = footRigidbody.gameObject.AddComponent<FixedJoint2D>();
            fixedJoint.connectedBody = collision.rigidbody;

            fixedJoint.autoConfigureConnectedAnchor = false;
            fixedJoint.enableCollision = true;

            //fixedJoint.anchor = foot.transform.localPosition;
            fixedJoint.frequency = 1f;
            fixedJoint.dampingRatio = 100000f;
        }
    }

    // Use this for initialization
    void Start () {
        leftLeg.Init(this);
        rightLeg.Init(this);

        rigidBody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        leftLeg.Update();
        rightLeg.Update();


        if(leftLeg.requestFreeze || rightLeg.requestFreeze)
        {
            rigidBody.constraints = RigidbodyConstraints2D.FreezeRotation;
        }
        else
        {
            rigidBody.constraints = RigidbodyConstraints2D.None;
        }

        if(leftLeg.isFixed || rightLeg.isFixed)
        {
            leftLeg.SetGravityScale(0);
            rightLeg.SetGravityScale(0);
            rigidBody.gravityScale = 0;
        }
        else
        {
            leftLeg.SetGravityScale(2);
            rightLeg.SetGravityScale(2);
            rigidBody.gravityScale = 2;
        }
    }
    
}
