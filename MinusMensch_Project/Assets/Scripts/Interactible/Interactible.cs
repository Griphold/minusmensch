﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

interface Interactible {

    UnityEvent Activated { get; }
    UnityEvent Deactivated { get; }
    bool IsActive { get; }
    
    void DebugActivated();
    void DebugDeactivated();
}
