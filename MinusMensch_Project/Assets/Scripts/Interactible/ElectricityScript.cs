﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class ElectricityScript : MonoBehaviour, Interactible {
    private HashSet<Collider2D> conductionList = new HashSet<Collider2D>();

    // UnityEvent for activation toggle
    private UnityEvent activated = new UnityEvent();
    public UnityEvent Activated {
        get {
            return activated;
        }
    }

    // UnityEvent for deactivation toggle
    private UnityEvent deactivated = new UnityEvent();
    public UnityEvent Deactivated {
        get {
            return deactivated;
        }
    }

    // Current state of the Interactible
    private bool isActive = false;
    public bool IsActive {
        get {
            return isActive;
        }
    }


    // Listener for Debugging
    private void Start() {
        activated.AddListener(DebugActivated);
        deactivated.AddListener(DebugDeactivated);
    }

    // Check if isActive is toggled 
    private void Update() {
        if (CheckForVoltage())
            if (isActive)
                activated.Invoke();
            else
                deactivated.Invoke();
    }

    // Updates the conductionList every Frame and checks if isActive toggled
    private bool CheckForVoltage() {
        bool isActiveTemp = isActive;
        isActive = false;
        HashSet<Collider2D> temp = new HashSet<Collider2D>();
        conductionList.Clear();
        conductionList.Add(gameObject.GetComponent<Collider2D>());

        do {
            conductionList.UnionWith(temp);
            temp.Clear();
            foreach (Collider2D go in conductionList) {
                SearchNearGameObjectsWithOverlap(go, temp);
                if (go.tag == "Positive")
                    isActive = true;
            }
        } while (temp.Count > 0);
        return isActive != isActiveTemp;
    }

    // Adds searcher toucher's to the found Set (used for CheckForVoltage)
    private void SearchNearGameObjects(Collider2D searcher, HashSet<Collider2D> found) {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(searcher.transform.position, searcher.bounds.extents.x * 2);
        foreach (Collider2D collider in colliders) {
            if (collider.IsTouching(searcher)
                && !conductionList.Contains(collider)) {
                found.Add(collider);
            }
        }
    }

    private void SearchNearGameObjectsWithOverlap(Collider2D searcher, HashSet<Collider2D> found) {
        float threshhold = 1.2f;
        Collider2D[] colliders;
        if (searcher.GetComponent<CircleCollider2D>() != null)
            colliders = Physics2D.OverlapCircleAll(searcher.transform.position, searcher.bounds.extents.x * threshhold);
        else if (searcher.GetComponent<BoxCollider2D>() != null)
            colliders = Physics2D.OverlapAreaAll(new Vector2(
                searcher.transform.position.x - searcher.bounds.extents.x * threshhold,
                searcher.transform.position.y - searcher.bounds.extents.y * threshhold
                ), new Vector2(
                searcher.transform.position.x + searcher.bounds.extents.x * threshhold,
                searcher.transform.position.y + searcher.bounds.extents.y * threshhold  
                ));
        else
            colliders = new Collider2D[] { };
        foreach (Collider2D collider in colliders) {
            if (!conductionList.Contains(collider)) {
                found.Add(collider);
            }
        }
    }


    // Just for Debugging
    public void DebugActivated() {
        Debug.Log("Activated!");
    }

    // Just for Debugging
    public void DebugDeactivated() {
        Debug.Log("Deactivated!");
    }
}