﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SuperButton : MonoBehaviour, Interactible {

    // UnityEvent for activation toggle
    private UnityEvent activated = new UnityEvent();
    public UnityEvent Activated {
        get {
            return activated;
        }
    }

    // UnityEvent for deactivation toggle
    private UnityEvent deactivated = new UnityEvent();
    public UnityEvent Deactivated {
        get {
            return deactivated;
        }
    }

    // Current state of the Interactible
    private bool isActive = false;
    public bool IsActive {
        get {
            return isActive;
        }
    }

    // Listener for Debugging
    private void Start() {
        activated.AddListener(DebugActivated);
        deactivated.AddListener(DebugDeactivated);
        gameObject.GetComponent<BoxCollider2D>().size = new Vector2(1.2f,1.0f);
        gameObject.GetComponent<BoxCollider2D>().offset = new Vector2(0.0f, 0.5f);
    }

    // Check if isActive is toggled on
    private void OnTriggerEnter(Collider col) {
        if (col.tag == "CompanionBall") {
            isActive = true;
            activated.Invoke();
        }
    }
    
    // Check if isActive is toggled off
    private void OnTriggerExit(Collider col) {
        if (col.tag == "CompanionBall") {
            isActive = false;
            deactivated.Invoke();
        }
    }


    // Just for Debugging
    public void DebugActivated() {
        Debug.Log("Activated!");
    }

    // Just for Debugging
    public void DebugDeactivated() {
        Debug.Log("Deactivated!");
    }
}
