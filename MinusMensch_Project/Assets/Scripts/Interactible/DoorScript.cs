﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour {
    [SerializeField]
    private GameObject[] activators;
    private Vector2 closedPosition;
    private Vector2 openPosition;
    //tells you wether to go up (true) or down (false)
    private bool isOpening;

    private void Start() {
        foreach (GameObject it in activators) {
            closedPosition = transform.position;
            openPosition = transform.position + Vector3.up * 2.0f;
            it.GetComponent<Interactible>().Activated.AddListener(ToggleActivation);
            it.GetComponent<Interactible>().Deactivated.AddListener(ToggleDeactivation);
        }
    }

    // returns true if every component in the array is activated
    public bool CheckActivation() {
        foreach (GameObject it in activators) {
            if (!it.GetComponent<Interactible>().IsActive)
                return false;
        }
        return true;
    }


    // Triggers once when every Iteractible in the array is activated
    private void ToggleActivation() {
        // interrupts when not everything else is active
        foreach (GameObject it in activators) {
            Debug.Log(it.tag + " " + it.GetComponent<Interactible>().IsActive);
            if (!it.GetComponent<Interactible>().IsActive) {
                return;
            }
        }
        isOpening = true;
    }

    // Triggers once when a Iteractible in the array is deactivated
    private void ToggleDeactivation() {
        foreach (GameObject it in activators) {
            if (!it.GetComponent<Interactible>().IsActive) {
            }
        }
        isOpening = false;
    }

    private void Update() {
        if (isOpening && transform.position.y < openPosition.y)
            transform.position += Vector3.up * Time.deltaTime * 3.0f;
        if (!isOpening && transform.position.y > closedPosition.y)
            transform.position -= Vector3.up * Time.deltaTime * 3.0f;
    }
}
